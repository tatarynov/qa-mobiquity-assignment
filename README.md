# Mobiquity QA challenge
## Tech stack:
- Java 8 ([Install docs](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html))
- Maven ([Install docs](https://maven.apache.org/guides/))
- Selenide (will be installed automatically)
- Cucumber (will be installed automatically)
- JUnit (will be installed automatically)
- AssertJ (will be installed automatically)
- Selenoid (with Docker) as Selenium grid server
- AWS as service provider for storage of running remote Selenoid server
- Please see pom.xml file for more details on application modules

## Features:
- Tests could be run locally or remotely on AWS based Selenoid server
- Tests run each feature in parallel (5 threads maximum)
- Browser drivers are downloaded automatically
- Chrome browser is used for tests by default

## How to set environment for running tests
Before executing tests there should be set next software:
- Java 8 ([Install docs](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html))
- Maven ([Install docs](https://maven.apache.org/guides/))
- Firefox if you need to run tests on this browser ([Install docs](https://www.mozilla.org/en-US/firefox/new/))
- Opera if you need to run on this browser ([Install docs](https://www.opera.com/))

#### Tests parameters

1. ```-Dcucumber.options='--tags @TagExample, @TagExample2``` -  set what tests should be run. 
Available options: ```@smoke, @create, @delete, @edit, @login, @logout```


2. ```-Dselenide.browser='chrome'``` -  set on which browser tests should be run. Default option is **Chrome**.
   Available options: ```chrome, firefox, safari, opera, ie```

### Assumptions
- You already familiar with: Git, Java, Maven, Terminal/Command line implementation of your Operation System
- Application code will be located in (Unix): ```~/Projects``` or in (Windows) ```C:\Projects```


Use this command to download code base
```bash
cd PROJECTS_DIR
git clone https://tatarynov@bitbucket.org/tatarynov/qa-mobiquity-assignment.git
```

Open terminal (Unix) or CMD prompt (MS Windows) and run following command:
```bash
cd PROJECT_DIR
```

## Run tests

Make sure that your application under test is running up and ready for testing.
Then run in directory with tests via command line/terminal:

```
mvn clean test
```
or with parameters:
```
mvn clean test -Dcucumber.options='--tags @smoke'
```

## Run tests on remote selenium server on AWS

To run tests using AWS-based Selenoid server use ```-Dremote``` parameter, e.g:
```
mvn clean test -Dremote=true
```

Then open your browser and navigate to link: [this link](http://ec2-18-191-216-228.us-east-2.compute.amazonaws.com:8080/)
You could monitor tests are running in real time.
For now Chrome only is added as supported browser to the  remote server. But other browsers could be added easily on demand.


## Assumptions made during tests

  - Edge cases are handled and error messages are displayed if data doesn’t meet requirements.
  
  - User friendly message is displayed if there is error on server side.
  
  -  Hint with creds is not displayed in real environment and won’t be tested
   
  - It's valid behavior that user can navigate back to login page after he is logged in
   


## Reports
Standard Cucumber report could be found after run in ```target/cucumber-report/report``` directory

The screenshots of failed tests could be found in ```build/reports/tests``` directory