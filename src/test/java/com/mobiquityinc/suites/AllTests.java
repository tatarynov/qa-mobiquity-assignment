package com.mobiquityinc.suites;

import com.codeborne.selenide.Selenide;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/report",
                "json:target/cucumber.json",
                "pretty"},
        features = "classpath:features",
        glue = "com.mobiquityinc.steps")
public class AllTests {

    @After
    public void setUp() {
        Selenide.close();
    }
}