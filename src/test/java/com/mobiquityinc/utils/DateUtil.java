package com.mobiquityinc.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    private static final String DATE_PATTERN = "yyyy-MM-dd";

    public static String getDate(TestDateFormat dateFormat) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        String date = "";
        switch (dateFormat) {
            case CURRENT:
                date = LocalDate.now().format(dateTimeFormatter);
                break;
            case IN_PAST:
                date = LocalDate.now().minusDays(1).format(dateTimeFormatter);
                break;
            case IN_FUTURE:
                date = LocalDate.now().plusDays(1).format(dateTimeFormatter);
                break;
            case MIN_POSSIBLE:
                date = LocalDate.of(1900, 1, 1).format(dateTimeFormatter);
                break;
            case MAX_POSSIBLE:
                date = LocalDate.of(2099, 12, 31).format(dateTimeFormatter);
                break;
        }
        return date;
    }
}
