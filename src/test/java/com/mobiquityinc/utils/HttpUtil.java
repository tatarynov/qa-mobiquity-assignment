package com.mobiquityinc.utils;


import com.google.gson.JsonObject;
import com.mobiquityinc.dto.Employee;
import com.mobiquityinc.steps.config.TestConfiguration;
import com.mobiquityinc.utils.exceptions.NotAuthorizedException;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class HttpUtil {

    private static final String HARDCODED_COOKIE_FOR_LOGIN = "_CafeTownsend-Angular-Rails_session=SW52bFpZSkJyeG1lWTFFSnZWVHVaTGZUeDJnV2szN2RUcE9Bdzlu" +
            "RGxwYUFybURUZEI5MGNUSU5xcTVGS21yZ0laSFdZMzZtUjFOUC9QMWRKWXQ3ekhseHp3UEw5SG5uSm03djBHUHNCMGNsc1" +
            "EwbTlYbW8xVzhISlVVRHlVTkVPZHgyYjJiMm5Ia2Y0czV2OVlNKzl0MG95WkVMeFNiS2JETCtuQjg4OUdoUVpyNzNiNmJ" +
            "JZzFhalVzQjNybUNsLS1lWmk0dGswSGk4VFRqT1lTTFE2RDVRPT0%3D--3b78aaf85b39108e875655ad539ed9e67376" +
            "041c";
    private static final String HARDCODED_TOKEN_FOR_LOGIN = "g6OBwuocsG+5AxmnUv2x+n6RX0tghOgDYFqp6KIR3x8=";
    private static HttpUtil instance;

    private String authCookie;

    public static synchronized HttpUtil getInstance() {
        if (instance == null) {
            instance = new HttpUtil();
        }
        return instance;
    }

    private HttpUtil() {
        RestAssured.baseURI = TestConfiguration.URL;
    }

    public String login(String username, String password) {
        JsonObject requestParams = new JsonObject();
        requestParams.addProperty("name", username);
        requestParams.addProperty("password", password);
        requestParams.addProperty("authorized", "false");

        RequestSpecification spec = given()
                .contentType(ContentType.JSON)
                .header("Cookie", HARDCODED_COOKIE_FOR_LOGIN)
                .header("X-CSRF-TOKEN", HARDCODED_TOKEN_FOR_LOGIN)
                .body(requestParams.toString());

        Response response = spec.post("/sessions");
        return authCookie = response.getHeader("Set-Cookie");
    }

    public List<Employee> getEmployees() {
        if (authCookie == null) throw new NotAuthorizedException("You are not authorized!");
        RequestSpecification spec = given()
                .cookie(authCookie);

        ResponseBody body = spec.get("/employees").getBody();
        return Arrays.asList(body.as(Employee[].class, ObjectMapperType.GSON));
    }

    public void deleteEmployee(long id) {
        if (authCookie == null) throw new NotAuthorizedException("You are not authorized!");
        RequestSpecification spec = given()
                .cookie(authCookie)
                .header("Cookie", HARDCODED_COOKIE_FOR_LOGIN)
                .header("X-CSRF-TOKEN", HARDCODED_TOKEN_FOR_LOGIN)
                .pathParam("id", id);

        spec.delete("/employees/{id}");
    }
}
