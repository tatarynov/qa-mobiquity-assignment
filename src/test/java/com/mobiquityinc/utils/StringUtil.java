package com.mobiquityinc.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.UUID;

import static java.lang.String.format;

public class StringUtil {

    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 255;
    private static final int MAX_LENGTH_FOR_EMAIL_LOCAL_PART = 186;
    private static final int MAX_LENGTH_FOR_EMAIL_DOMAIN_PART = 64;
    private static final int MAX_LENGTH_FOR_EMAIL_DOMAIN_ZONE = 4;

    public static final String TEST_UNIQUE_ID = "qa-mob-test";

    public static String getShortName() {
        return RandomStringUtils.randomAlphanumeric(MIN_LENGTH);
    }

    public static String getLongName() {
        return TEST_UNIQUE_ID + RandomStringUtils.randomAlphanumeric(MAX_LENGTH - TEST_UNIQUE_ID.length());
    }

    public static String getInvalidLongName() {
        return TEST_UNIQUE_ID + RandomStringUtils.randomAlphanumeric(MAX_LENGTH - TEST_UNIQUE_ID.length() + 1);
    }

    public static String getAnyValidValue() {
        return TEST_UNIQUE_ID + UUID.randomUUID().toString();
    }


    public static String getValidEmail() {
        String pattern = "%s@%s.%s";
        String localPart = RandomStringUtils.randomAlphanumeric(MIN_LENGTH, MAX_LENGTH_FOR_EMAIL_LOCAL_PART);
        String domainPart = RandomStringUtils.randomAlphanumeric(MIN_LENGTH, MAX_LENGTH_FOR_EMAIL_DOMAIN_PART);
        String domainZone = RandomStringUtils.randomAlphanumeric(MIN_LENGTH, MAX_LENGTH_FOR_EMAIL_DOMAIN_ZONE);
        return format(pattern, localPart, domainPart, domainZone);
    }
}
