package com.mobiquityinc.utils;

public enum TestDateFormat {

    CURRENT, IN_FUTURE, IN_PAST, MIN_POSSIBLE, MAX_POSSIBLE, ANY
}
