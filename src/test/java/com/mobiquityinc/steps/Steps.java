package com.mobiquityinc.steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.mobiquityinc.pages.CreateEmployeePage;
import com.mobiquityinc.pages.EditEmployeePage;
import com.mobiquityinc.pages.ListEmployeesPage;
import com.mobiquityinc.pages.LoginPage;
import com.mobiquityinc.utils.DateUtil;
import com.mobiquityinc.utils.StringUtil;
import com.mobiquityinc.utils.TestDateFormat;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Arrays;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Selenide.open;
import static com.mobiquityinc.utils.StringUtil.getValidEmail;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class Steps {

    private LoginPage loginPage;
    private ListEmployeesPage listEmployeesPage;
    private CreateEmployeePage createEmployeePage;
    private EditEmployeePage editEmployeePage;

    private String username;
    private String firstName;
    private String lastName;
    private String startDate;
    private String email;

    @Given("Open login page")
    public void openMainPage() {
        open("/");
        loginPage = new LoginPage();
    }

    @And("Fill in first name with {string} value")
    public void fillInFirstNameWithFirst_nameValue(String firstName) {
        this.firstName = getValue(firstName);
        createEmployeePage.setFirstName(this.firstName);
    }

    @And("Fill in last name with {string} value")
    public void fillLastName(String lastName) {
        this.lastName = getValue(lastName);
        createEmployeePage.setLastName(this.lastName);
    }

    @And("Fill in username with {string} value")
    public void fillInUsernameWithLukeValue(String userName) {
        loginPage.fillUsername(userName);
        this.username = userName;
    }

    @And("Fill in password with {string} value")
    public void fillInPasswordWithSkywalkerValue(String password) {
        loginPage.fillPassword(password);
    }

    @And("Click on Sign in button")
    public void clickOnSignInButton() {
        listEmployeesPage = loginPage.submit();
    }

    @When("Click on {string} button on List of Employees page")
    public void clickOnCreateButtonOnListOfEmployeesPage(String val) {
        final String buttonName = val.toLowerCase();
        if (buttonName.equals("create")) {
            listEmployeesPage.create();
            createEmployeePage = new CreateEmployeePage();
        } else if (buttonName.equals("edit")) {
            listEmployeesPage.edit();
            editEmployeePage = new EditEmployeePage();
        } else if (buttonName.equals("delete")) {
            listEmployeesPage.delete();
        }
    }

    @And("Fill in start date with {string} value")
    public void fillInStartDateWithValue(String val) {
        boolean isEnum = Arrays.stream(TestDateFormat.values()).anyMatch(v -> v.name().equals(val));
        if (isEnum) {
            TestDateFormat dateFormat = TestDateFormat.valueOf(val);
            createEmployeePage.setStartDate(startDate = DateUtil.getDate(dateFormat));
        } else {
            createEmployeePage.setStartDate(startDate = val);
        }
    }

    @And("Fill in start date with any valid value")
    public void fillInStartDateWithValue() {
        createEmployeePage.setStartDate(startDate = DateUtil.getDate(TestDateFormat.CURRENT));
    }

    @And("Fill in email with {string} value")
    public void fillInEmailWithValidEmailComValue(String email) {
        this.email = email;
        createEmployeePage.setEmail(email);
    }

    @Then("Page with List of employees is displayed")
    public void pageWithListOfEmployeesIsDisplayed() {
        assertThat(listEmployeesPage.employeesListShouldBe(Condition.visible))
                .as("List of employees is displayed")
                .isTrue();

        assertThat(listEmployeesPage.isGreetingDisplayedFor(username))
                .as("Username with greeting is displayed")
                .isTrue();
    }

    @And("Refresh page")
    public void refreshPage() {
        Selenide.refresh();
    }

    @And("Go back")
    public void goBack() {
        Selenide.back();
    }

    @Then("Main page is displayed")
    public void mainPageIsDisplayed() {
        assertThat(loginPage.isDisplayed())
                .as("Login page is opened and displayed")
                .isTrue();
    }

    @And("Go forward")
    public void goForward() {
        Selenide.forward();
    }

    @When("Click on username field")
    public void clickOnUsernameField() {
        loginPage.clickOnUsernameField();
    }

    @And("Hover to prompt icon")
    public void hoverToPromptIcon() {
        // TODO: mocked method
    }

    @Then("{string} message is displayed")
    public void isMessageIsDisplayed(String message) {
        loginPage.isMessageDisplayed(message);
    }

    @When("Click on password field")
    public void clickOnPasswordField() {
        loginPage.clickOnPasswordField();
    }

    @Then("Message {string} is displayed")
    public void messageIsDisplayed(String message) {
        // TODO: mocked method
    }

    @Then("Footer is displayed with next content: {string}")
    public void footerIsDisplayedWithNextContent(String footerContent) {
        assertThat(loginPage.getFooterContent().contains(footerContent))
                .as("Footer contains {}", footerContent)
                .isTrue();
    }

    @Then("Fields do not contain any warnings")
    public void fieldsDoNotContainAnyWarnings() {
        loginPage.verifyWarningsForUsername();
        loginPage.verifyWarningsForPassword();
    }

    @And("Click on Add button")
    public void clickOnAddButton() {
        assertThat(createEmployeePage.add())
                .as("No alerts displayed during adding Employee")
                .isTrue();
    }

    @Then("Entry with {string} and {string} is created")
    public void isEntryCreated(String firstname, String lastname) {
        String fullname = format("%s %s", firstname, lastname);
        assertThat(listEmployeesPage.isEntryDisplayedWith(fullname))
                .as("Entry is successfully created")
                .isTrue();
    }

    @And("Fill in first name with any valid value")
    public void fillInFirstNameWithAnyValidValue() {
        createEmployeePage.setFirstName(this.firstName = StringUtil.getAnyValidValue());
    }

    @And("Fill in last name with any valid value")
    public void fillInLastNameWithAnyValidValue() {
        createEmployeePage.setLastName(this.lastName = StringUtil.getAnyValidValue());
    }

    @Then("Entry is successfully created and displayed in list of employees")
    public void entryIsSuccessfullyCreated() {
        isEntryCreated(firstName, lastName);
    }

    @And("Fill in email with any valid value")
    public void fillInEmailWithAnyValidValue() {
        createEmployeePage.setEmail(this.email = getValidEmail());
    }

    @And("Contains entered data")
    public void containsEnteredData() {
        editEmployeePage = listEmployeesPage.doubleClickOnName(this.firstName + " " + this.lastName);
        assertThat(editEmployeePage.getFirstName()).isEqualTo(firstName);
        assertThat(editEmployeePage.getLastName()).isEqualTo(lastName);
        assertThat(editEmployeePage.getStartDate()).isEqualTo(startDate);
        assertThat(editEmployeePage.getEmail()).isEqualTo(email);
    }

    @Then("Entry is not created and Create Employee page is still displayed")
    public void entryIsNotCreatedAndCreateEmployeePageIsStillDisplayed() {
        boolean isVisible = listEmployeesPage.employeesListShouldBe(not(Condition.visible));
        assertThat(isVisible)
                .as("List of Employees is not visible")
                .isFalse();
        assertThat(createEmployeePage.isDisplayed())
                .as("Create Employee page is displayed!")
                .isTrue();
    }

    private String getValue(String value) {
        switch (value) {
            case "RANDOM_SHORT_NAME":
                return StringUtil.getShortName();
            case "RANDOM_LONG_NAME":
                return StringUtil.getLongName();
            case "RANDOM_LONG_INVALID_NAME":
                return StringUtil.getInvalidLongName();
            default:
                return value;
        }
    }

    @When("Select newly created entry")
    public void selectNewlyCreatedEntry() {
        // TODO: mocked method
    }

    @And("Click on Edit button")
    public void clickOnEditButton() {
        // TODO: mocked method
    }

    @Then("User is on Edit Employee page")
    public void userIsOnEditEmployeePage() {
        // TODO: mocked method
    }

    @When("Double click on newly created entry")
    public void doubleClickOnNewlyCreatedEntry() {
        // TODO: mocked method
    }

    @And("Update first name with {string} value")
    public void updateFirstNameWithFirst_nameValue(String firstName) {
        // TODO: mocked method
    }

    @And("Update last name with {string} value")
    public void updateLastNameWithLast_nameValue(String lastName) {
        // TODO: mocked method
    }

    @And("Update start date with {string} value")
    public void updateStartDateWithStart_dateValue(String startDate) {
        // TODO: mocked method
    }

    @And("Update email with any <email> value")
    public void updateEmailWithAnyEmailValue() {
        // TODO: mocked method
    }

    @And("Click on Update button")
    public void clickOnUpdateButton() {
        // TODO: mocked method
    }

    @And("Update first name with any valid value")
    public void updateFirstNameWithAnyValidValue() {
        // TODO: mocked method
    }

    @And("Update last name with any valid value")
    public void updateLastNameWithAnyValidValue() {
        // TODO: mocked method
    }

    @And("Update start date with any valid value")
    public void updateStartDateWithAnyValidValue() {
        // TODO: mocked method
    }

    @And("Update email with any valid value")
    public void updateEmailWithAnyValidValue() {
        // TODO: mocked method
    }

    @Then("Entry is successfully updated and displayed in list of employees with new values")
    public void entryIsSuccessfullyUpdatedAndDisplayedInListOfEmployeesWithNewValues() {
        // TODO: mocked method
    }

    @When("Double click on newly updated entry")
    public void doubleClickOnNewlyUpdatedEntry() {
        // TODO: mocked method
    }

    @And("Click on Back button")
    public void clickOnBackButton() {
        // TODO: mocked method
    }

    @Then("Entry is not updated and displayed in list of employees with previous values")
    public void entryIsNotUpdatedAndDisplayedInListOfEmployeesWithPreviousValues() {
        // TODO: mocked method
    }

    @And("Update email with {string} value")
    public void updateEmailWithEmailValue(String email) {
        // TODO: mocked method
    }
}
