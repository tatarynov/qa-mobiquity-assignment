package com.mobiquityinc.steps.config;

import com.codeborne.selenide.Configuration;
import com.mobiquityinc.pages.EditEmployeePage;
import com.mobiquityinc.utils.HttpUtil;
import com.mobiquityinc.utils.StringUtil;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.remote.CapabilityType;

import java.util.stream.Collectors;

public class TestConfiguration {

    public static final String URL = "http://cafetownsend-angular-rails.herokuapp.com";
    private static final String AWS_SELENIUM_SERVER = "http://ec2-18-191-216-228.us-east-2.compute.amazonaws.com:4444/wd/hub";
    private static final int IMPLICIT_TIMEOUT = 10000;

    private static boolean isClean = false;
    private static HttpUtil httpUtil = HttpUtil.getInstance();

    private static final String USERNAME = "Luke";
    private static final String PASSWORD = "Skywalker";


    @Before(order = 1)
    public static void configuration() {
        // put all your configs here
        Configuration.baseUrl = URL;
        Configuration.timeout = IMPLICIT_TIMEOUT;

        // capability to ignore unexpected alerts due to bugs
        Configuration.browserCapabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                UnexpectedAlertBehaviour.IGNORE);

        // configuration for remote server
        if (Boolean.valueOf(System.getProperty("remote", "")).equals(true)) {
            Configuration.remote = AWS_SELENIUM_SERVER;
            Configuration.browserCapabilities.setCapability("enableVNC", true);
        }
    }

    @After(order = 2)
    public static synchronized void cleanEnv() {
        httpUtil.login(USERNAME, PASSWORD);
        httpUtil.getEmployees()
                .stream()
                .filter(v -> v.getFirstName().contains(StringUtil.TEST_UNIQUE_ID))
                .collect(Collectors.toList())
                .forEach(v -> httpUtil.deleteEmployee(v.getId()));
    }

    @After("@wipe_on_ui")
    public static void tearDown() {
        new EditEmployeePage().delete();
    }
}
