@edit @TBD
Feature: Edit user

  Background:
    Given Open login page
    And Fill in username with 'Luke' value
    And Fill in password with 'Skywalker' value
    And Click on Sign in button
    And Click on 'Create' button on List of Employees page
    And Fill in first name with any valid value
    And Fill in last name with any valid value
    And Fill in start date with any valid value
    And Fill in email with any valid value
    And Click on Add button

    ############# POSITIVE SCENARIOS ###############

  @P0 @smoke
  Scenario: Select entry and click Edit button
    When Select newly created entry
    And Click on Edit button
    Then User is on Edit Employee page

  @P0 @smoke
  Scenario: Double-click to open profile
    When Double click on newly created entry
    Then User is on Edit Employee page

  @P0
  Scenario Outline: Update names with valid values
    When Double click on newly created entry
    And Update first name with '<first_name>' value
    And Update last name with '<last_name>' value
    And Update start date with '<start_date>' value
    And Update email with '<email>' value
    And Click on Update button
    Then Entry is successfully created and displayed in list of employees
    And Contains entered data

    Examples:
      | first_name        | last_name         | start_date   | email                         |
      | RANDOM_SHORT_NAME | RANDOM_SHORT_NAME | CURRENT      | email@domain.com              |
      | O`Hara 1          | Jr.Princö 2       | IN_FUTURE    | firstname.lastname@domain.com |
      | ëøœßšäå           | ëøœßšäå           | IN_PAST      | email@subdomain.domain.com    |
      | lowercase         | UPPERCASE         | MIN_POSSIBLE | firstname+lastname@domain.com |
      | UPPERCASE         | lowercase         | MAX_POSSIBLE | email@123.123.123.123         |
      | !@#$%^&*()-=      | !@#$%^&*()-=      | 2014-02-29   | email@domain-one.com          |
      | кириллица         | кириллица         | CURRENT      | _______@domain.com            |
      | RANDOM_LONG_NAME  | RANDOM_LONG_NAME  | CURRENT      | 1234567890@domain.com         |

  @P1
  Scenario: Change values and go back
    When Double click on newly created entry
    And Update first name with any valid value
    And Update last name with any valid value
    And Update start date with any valid value
    And Update email with any valid value
    And Click on Update button
    Then Entry is successfully updated and displayed in list of employees with new values
    When Double click on newly updated entry
    Then Contains entered data

  @P1
  Scenario: Update data to match already existing employee

  @P1
  Scenario: Do not change values and go back
    When Double click on newly created entry
    And Update first name with any valid value
    And Update last name with any valid value
    And Update start date with any valid value
    And Update email with any valid value
    And Click on Back button
    Then Entry is not updated and displayed in list of employees with previous values
    When Double click on newly updated entry
    Then Contains entered data

  @P2
  Scenario: Try to update without changes
    When Double click on newly created entry
    And Click on Update button
    Then Entry is not updated and displayed in list of employees with previous values
    When Double click on newly updated entry
    Then Contains entered data

    ############# NEGATIVE SCENARIOS ###############

  @P2 @names
  Scenario Outline: Verify invalid first and last names
    When Double click on newly created entry
    And Update first name with '<first_name>' value
    And Update last name with '<last_name>' value
    And Update start date with any valid value
    And Update email with any valid value
    And Click on Update button
    Then User is on Edit Employee page
    And Message '<message>' is displayed

    Examples:
      | first_name               | last_name                | message                     |
      # empty value
      |                          |                          | Please fill out this field. |
      | RANDOM_SHORT_NAME        |                          | Please fill out this field. |
      |                          | RANDOM_SHORT_NAME        | Please fill out this field. |
      # whitespaces
      |                          |                          | Please fill out this field. |
      # long invalid values (256 chars)
      | RANDOM_LONG_INVALID_NAME | RANDOM_LONG_INVALID_NAME | Please fill out this field. |

  @P2 @start_date
  Scenario Outline: Verify invalid start date
    When Double click on newly created entry
    And Update first name with any valid value
    And Update last name with any valid value
    And Update start date with '<start_date>' value
    And Update email with any valid value
    And Click on Update button
    Then User is on Edit Employee page
    And Message '<message>' is displayed

    Examples:
      | start_date | message                     |
      # empty value
      |            | Please fill out this field. |
      # invalid format
      | 1990-31-12 | Please fill out this field. |
      | 12-31-1900 | Please fill out this field. |
      | 31-12-1900 | Please fill out this field. |
      | 1991-13-10 | Please fill out this field. |
      | 1900-12-32 | Please fill out this field. |
      | 2000-1-1   | Please fill out this field. |
      # leap year
      | 2016-02-30 | Please fill out this field. |
      # values outside valid range
      | 1899-12-31 | Please fill out this field. |
      | 2100-01-01 | Please fill out this field. |

  @P2 @email
  Scenario Outline: Verify invalid email
    When Double click on newly created entry
    And Update first name with any valid value
    And Update last name with any valid value
    And Update start date with any valid value
    And Update email with '<email>' value
    And Click on Update button
    Then User is on Edit Employee page
    And Message '<message>' is displayed

    Examples:
      | email                        | message       |
      | plainaddress                 | to be defined |
      | #@%^%#$@#$@#.com             | to be defined |
      | @domain.com                  | to be defined |
      | Joe Smith <email@domain.com> | to be defined |
      | email.domain.com             | to be defined |
      | email@domain@domain.com      | to be defined |
      | .email@domain.com            | to be defined |
      | email.@domain.com            | to be defined |
      | email..email@domain.com      | to be defined |
      | あいうえお@domain.com             | to be defined |
      | email@domain.com (Joe Smith) | to be defined |


