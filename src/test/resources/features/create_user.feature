@create
Feature: Create user

  # ASSUMPTIONS:
  # -- Edge cases are handled and error messages are displayed if data doesn’t meet requirements.
  # -- User friendly message is displayed if user tries to send 256 char length values

  Background:
    Given Open login page
    And Fill in username with 'Luke' value
    And Fill in password with 'Skywalker' value
    And Click on Sign in button

    ############# POSITIVE SCENARIOS ###############

  @P0 @smoke @names @wipe_on_ui
  Scenario Outline: Verify valid first and last names
    When Click on 'Create' button on List of Employees page
    And Fill in first name with '<first_name>' value
    And Fill in last name with '<last_name>' value
    And Fill in start date with any valid value
    And Fill in email with any valid value
    And Click on Add button
    Then Entry is successfully created and displayed in list of employees
    And Contains entered data

    Examples:
      | first_name        | last_name         |
      # min valid value
      | RANDOM_SHORT_NAME | RANDOM_SHORT_NAME |
      # valid names divided by equivalent classes
      | O`Hara 1          | Jr.Princö 2       |
      | ëøœßšäå           | ëøœßšäå           |
      | lowercase         | UPPERCASE         |
      | UPPERCASE         | lowercase         |
      | !@#$%^&*()-=      | !@#$%^&*()-=      |
      | кириллица         | кириллица         |
      | ἱερογλύφος漢字      | ἱερογλύφος漢字      |
      # long values
      | RANDOM_LONG_NAME  | RANDOM_LONG_NAME  |

  @P0 @smoke @start_date
  Scenario Outline: Verify valid start date
    When Click on 'Create' button on List of Employees page
    And Fill in first name with any valid value
    And Fill in last name with any valid value
    And Fill in start date with '<start_date>' value
    And Fill in email with any valid value
    And Click on Add button
    Then Entry is successfully created and displayed in list of employees
    And Contains entered data

    Examples:
      | start_date   |
      | CURRENT      |
      | IN_FUTURE    |
      | IN_PAST      |
      | MIN_POSSIBLE |
      | MAX_POSSIBLE |
      # leap year
      | 2016-02-29   |


  @P0 @smoke @email
  Scenario Outline: Verify valid email
    When Click on 'Create' button on List of Employees page
    And Fill in first name with any valid value
    And Fill in last name with any valid value
    And Fill in start date with any valid value
    And Fill in email with '<email>' value
    And Click on Add button
    Then Entry is successfully created and displayed in list of employees
    And Contains entered data

    Examples:
      | email                         |
      | email@domain.com              |
      | firstname.lastname@domain.com |
      | email@subdomain.domain.com    |
      | firstname+lastname@domain.com |
      | email@123.123.123.123         |
      | 1234567890@domain.com         |
      | email@domain-one.com          |
      | _______@domain.com            |
      | email@domain.name             |
      | email@domain.co.jp            |
      | firstname-lastname@domain.com |

  @P1 @smoke
  Scenario: Fill the fields and go back without saving

  @P1 @smoke
  Scenario: Create account with already registered values

  @P2
  Scenario: Check prompts for every field


############# NEGATIVE SCENARIOS ###############

  @P2 @names
  Scenario Outline: Verify invalid first and last names
    When Click on 'Create' button on List of Employees page
    And Fill in first name with '<first_name>' value
    And Fill in last name with '<last_name>' value
    And Fill in start date with any valid value
    And Fill in email with any valid value
    And Click on Add button
    Then Entry is not created and Create Employee page is still displayed
    Then Message '<message>' is displayed

    Examples:
      | first_name               | last_name                | message                     |
      # empty value
      |                          |                          | Please fill out this field. |
      | RANDOM_SHORT_NAME        |                          | Please fill out this field. |
      |                          | RANDOM_SHORT_NAME        | Please fill out this field. |
      # whitespaces
      |                          |                          | Please fill out this field. |
      # long invalid values (256 chars)
      | RANDOM_LONG_INVALID_NAME | RANDOM_LONG_INVALID_NAME | Please fill out this field. |

  @P2 @start_date
  Scenario Outline: Verify invalid start date
    When Click on 'Create' button on List of Employees page
    And Fill in first name with any valid value
    And Fill in last name with any valid value
    And Fill in start date with '<start_date>' value
    And Fill in email with any valid value
    And Click on Add button
    Then Entry is not created and Create Employee page is still displayed
    Then Message '<message>' is displayed

    Examples:
      | start_date | message                     |
      # empty value
      |            | Please fill out this field. |
      # invalid format
      | 1990-31-12 | Please fill out this field. |
      | 12-31-1900 | Please fill out this field. |
      | 31-12-1900 | Please fill out this field. |
      | 1991-13-10 | Please fill out this field. |
      | 1900-12-32 | Please fill out this field. |
      | 2000-1-1   | Please fill out this field. |
      # leap year
      | 2016-02-30 | Please fill out this field. |
      # values outside valid range
      | 1899-12-31 | Please fill out this field. |
      | 2100-01-01 | Please fill out this field. |

  @P2 @email
  Scenario Outline: Verify invalid email
    When Click on 'Create' button on List of Employees page
    And Fill in first name with any valid value
    And Fill in last name with any valid value
    And Fill in start date with any valid value
    And Fill in email with '<email>' value
    And Click on Add button
    Then Entry is not created and Create Employee page is still displayed
    Then Message '<message>' is displayed

    Examples:
      | email                        | message       |
      | plainaddress                 | to be defined |
      | #@%^%#$@#$@#.com             | to be defined |
      | @domain.com                  | to be defined |
      | Joe Smith <email@domain.com> | to be defined |
      | email.domain.com             | to be defined |
      | email@domain@domain.com      | to be defined |
      | .email@domain.com            | to be defined |
      | email.@domain.com            | to be defined |
      | email..email@domain          | to be defined |
      | あいうえお@domain.com         | to be defined |
      | email@domain.com (Joe Smith) | to be defined |




