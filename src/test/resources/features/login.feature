@login
Feature: Login

  # ASSUMPTIONS:

  # -  Hint with creds is not displayed in real environment and won’t be tested
  # - It's valid behavior that user can navigate back to login page after login
  # - Pre-conditions with registration new user are not implemented for now

  Background:
    Given Open login page

  @P0 @smoke
  Scenario: Log in with valid credentials
    When Fill in username with 'Luke' value
    And Fill in password with 'Skywalker' value
    And Click on Sign in button
    Then Page with List of employees is displayed

  @P0 @smoke
  Scenario: Session storage after login
    When Fill in username with 'Luke' value
    And Fill in password with 'Skywalker' value
    And Click on Sign in button
    And Page with List of employees is displayed
    And Refresh page
    Then Page with List of employees is displayed

  @P1
  Scenario: Navigate back after login
    When Fill in username with 'Luke' value
    And Fill in password with 'Skywalker' value
    And Click on Sign in button
    Then Page with List of employees is displayed
    When Go back
    Then Main page is displayed
    And Go forward
    And Page with List of employees is displayed

  @P2
  Scenario: Check prompts for credentials fields
    When Click on username field
    And Hover to prompt icon
    Then 'Please fill out this field.' message is displayed

    When Click on password field
    And Hover to prompt icon
    Then 'Please fill out this field.' message is displayed

  @P2
  Scenario Outline: Log in with invalid credentials
    When Fill in username with '<username>' value
    And Fill in password with '<password>' value
    And Click on Sign in button
    Then Message '<error_message>' is displayed
    Examples:
      | username       | password             | error_message                 |
      # empty values
      |                |                      | Please fill out this field.   |
      |                | Skywalker            | Please fill out this field.   |
      | Luke           |                      | Please fill out this field.   |
      # invalid values
      | Not existing   | User                 | Invalid username or password! |
      | Luke           | Wrong password       | Invalid username or password! |
      | Wrong username | Skywalker            | Invalid username or password! |
      | luke           | skywalker            | Invalid username or password! |
      # SQL injection
      | Luke           | Skywalker;`OR 1=1 -- | Invalid username or password! |

  @P2
  Scenario: Check footer content
    Then Footer is displayed with next content: 'Built with AngularJS, CoffeeScript, Bourbon and Rails.'

  @P2
  Scenario: Check warnings
    Then Fields do not contain any warnings

  @P2
  Scenario: Check that Edit and Delete buttons are blocked just after login



