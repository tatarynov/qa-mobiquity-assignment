package com.mobiquityinc.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.switchTo;

public class EditEmployeePage extends CreateEmployeePage {

    private final By deleteButton = By.xpath("//p[text()='Delete']");

    public ListEmployeesPage /**/delete() {
        $(deleteButton).click();
        switchTo().alert().accept();
        return new ListEmployeesPage();
    }

    public String getFirstName() {
        return $(firstName).val();
    }

    public String getLastName() {
        return $(lastName).val();
    }

    public String getStartDate() {
        return $(startDate).val();
    }

    public String getEmail() {
        return $(email).val();
    }
}
