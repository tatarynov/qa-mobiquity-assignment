package com.mobiquityinc.pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends BasePage {

    private final By username = By.cssSelector("[ng-model='user.name']");
    private final By password = By.cssSelector("[ng-model='user.password']");
    private final By loginButton = By.cssSelector("button[type='submit']");
    private final By footer = By.cssSelector("footer");

    public LoginPage fillUsername(String val) {
        $(username).sendKeys(val);
        return this;
    }

    public LoginPage fillPassword(String val) {
        $(password).sendKeys(val);
        return this;
    }

    public ListEmployeesPage submit() {
        $(loginButton).click();
        return new ListEmployeesPage();
    }

    public String getFooterContent() {
        return $(footer).getText();
    }

    public boolean isDisplayed() {
        return $(username).shouldBe(Condition.visible).isDisplayed() &&
                $(password).isDisplayed() &&
                $(loginButton).isDisplayed();
    }

    public LoginPage clickOnUsernameField() {
        $(username).click();
        return this;
    }

    public LoginPage clickOnPasswordField() {
        $(password).click();
        return this;
    }

    public void isMessageDisplayed(String message) {
        $("body").shouldHave(Condition.text(message));
    }

    public void verifyWarningsForUsername() {
        $(username).shouldNotHave(Condition.cssClass("ng-invalid"));
    }

    public void verifyWarningsForPassword() {
        $(username).shouldNotHave(Condition.cssClass("ng-invalid"));
    }
}
