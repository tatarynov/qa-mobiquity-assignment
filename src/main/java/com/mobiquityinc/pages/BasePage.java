package com.mobiquityinc.pages;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.NoAlertPresentException;

/***
 * Class for containing common methods for all pages
 *
 */
public class BasePage {

    private static final int ALERT_TIMEOUT = 2000;

    /***
     * Verifies if alert is displayed
     *
     * @return true if alert is not displayed
     */
    boolean isAlertDisplayed() {
        long start = System.currentTimeMillis();
        while ((System.currentTimeMillis() - start) < ALERT_TIMEOUT) {
            try {
                WebDriverRunner.getWebDriver().switchTo().alert().accept();
                return true;
            } catch (NoAlertPresentException ignored) {
            }
        }
        return false;
    }
}
