package com.mobiquityinc.pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static java.lang.String.format;

public class ListEmployeesPage extends BasePage {

    private final By usernameElement = By.cssSelector("#greetings");
    private final By employeesList = By.cssSelector("#employee-list");
    private final By createButton = By.cssSelector("#bAdd");
    private final By editButton = By.cssSelector("#bEdit");
    private final By deleteButton = By.cssSelector("#bDelete");

    private static final String GREETINGS_MESSAGE = "Hello %s";
    private static final String EMPLOYEE_ENTRY_PATTERN = "//li[contains(.,'%s')]";


    public boolean employeesListShouldBe(Condition condition) {
        return $(employeesList).should(condition).exists();
    }

    public boolean isGreetingDisplayedFor(String username) {
        return $(usernameElement)
                .getText()
                .equals((format(GREETINGS_MESSAGE, username)));
    }

    public void create() {
        $(createButton).click();
    }

    public void edit() {
        $(editButton).click();
    }

    public void delete() {
        $(deleteButton).click();
    }

    public EditEmployeePage doubleClickOnName(String name) {
        new Actions(getWebDriver())
                .doubleClick($(getEmployeeLocator(name)))
                .perform();

        return new EditEmployeePage();
    }

    public boolean isEntryDisplayedWith(String name) {
        return $(getEmployeeLocator(name))
                .isDisplayed();
    }

    public ListEmployeesPage deleteEmployee(String name) {
        By employeeLocator = getEmployeeLocator(name);
        $(employeeLocator).click();
        delete();
        switchTo().alert().accept();

        $(employeeLocator).shouldNot(Condition.be(Condition.exist));
        return this;
    }

    public ListEmployeesPage deleteEmployees(String name) {
        employeesListShouldBe(Condition.visible);
        By employeeLocator = getEmployeeLocator(name);
        SelenideElement element;
        while ((element = $(employeeLocator)).shouldBe(Condition.visible).exists()) {
            element.click();
            delete();
            switchTo().alert().accept();
            element.shouldNot(Condition.be(Condition.visible));
            element.shouldNot(Condition.be(Condition.exist));
        }

        return this;
    }

    private By getEmployeeLocator(String name) {
        return By.xpath(format(EMPLOYEE_ENTRY_PATTERN, name));
    }
}
