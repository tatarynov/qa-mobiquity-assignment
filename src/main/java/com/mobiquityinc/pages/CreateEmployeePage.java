package com.mobiquityinc.pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CreateEmployeePage extends BasePage {

    protected final By firstName = By.cssSelector("[ng-model=\"selectedEmployee.firstName\"]");
    protected final By lastName = By.cssSelector("[ng-model=\"selectedEmployee.lastName\"]");
    protected final By startDate = By.cssSelector("[ng-model=\"selectedEmployee.startDate\"]");
    protected final By email = By.cssSelector("[ng-model=\"selectedEmployee.email\"]");

    private final By addButton = By.xpath("//button[text()='Add']");

    public CreateEmployeePage setFirstName(String val) {
        $(firstName).sendKeys(val);
        return this;
    }

    public CreateEmployeePage setLastName(String val) {
        $(lastName).sendKeys(val);
        return this;
    }

    public CreateEmployeePage setStartDate(String val) {
        $(startDate).sendKeys(val);
        return this;
    }

    public CreateEmployeePage setEmail(String val) {
        $(email).sendKeys(val);
        return this;
    }

    public boolean add() {
        $(addButton).click();
        return !isAlertDisplayed();
    }

    public boolean isDisplayed() {
        return $(firstName).exists() &&
                $(lastName).exists() &&
                $(startDate).exists() &&
                $(email).exists();
    }
}
